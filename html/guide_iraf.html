
<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="generator" content="Docutils 0.19: https://docutils.sourceforge.io/" />

    <title>Guide for IRAF users &#8212; pyspeckit v1.0.2.dev3084</title>
    <link rel="stylesheet" type="text/css" href="_static/pygments.css" />
    <link rel="stylesheet" type="text/css" href="_static/extra.css" />
    <link rel="stylesheet" type="text/css" href="_static/graphviz.css" />
    <link rel="stylesheet" type="text/css" href="_static/plot_directive.css" />
    <script src="_static/jquery.js"></script>
    <script src="_static/_sphinx_javascript_frameworks_compat.js"></script>
    <script data-url_root="./" id="documentation_options" src="_static/documentation_options.js"></script>
    <script src="_static/doctools.js"></script>
    <script src="_static/sphinx_highlight.js"></script>
    <link rel="icon" href="_static/logo.ico"/>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="PySpecKit Projects" href="projects.html" />
    <link rel="prev" title="Guide for GILDAS-CLASS users" href="guide_class.html" /> 
  </head><body>
    <div class="header-wrapper">
      <div class="header">
        <div class="headertitle"><a
          href="index.html">pyspeckit v1.0.2.dev3084</a></div>
        <div class="rel">
          <a href="guide_class.html" title="Guide for GILDAS-CLASS users"
             accesskey="P">previous</a> |
          <a href="projects.html" title="PySpecKit Projects"
             accesskey="N">next</a> |
          <a href="py-modindex.html" title="Python Module Index"
             >modules</a> |
          <a href="genindex.html" title="General Index"
             accesskey="I">index</a>
        </div>
       </div>
    </div>

    <div class="content-wrapper">
      <div class="content">
        <div class="document">
            
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <section id="guide-for-iraf-users">
<h1>Guide for IRAF users<a class="headerlink" href="#guide-for-iraf-users" title="Permalink to this heading">¶</a></h1>
<p>PySpecKit is similar in intent and implementation to IRAF’s <em>splot</em> routine.
IRAF users will probably want to do most of their data reduction (i.e., turning
an image/cube into a 1D wavelength-calibrated spectrum) in IRAF, but will be
comfortable fitting lines and making publication-quality plots using PySpecKit.</p>
<section id="loading-a-spectrum">
<h2>Loading a Spectrum<a class="headerlink" href="#loading-a-spectrum" title="Permalink to this heading">¶</a></h2>
<p>If you have an IRAF spectrum, it is straightforward to load into PySpecKit:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="n">sp</span> <span class="o">=</span> <span class="n">pyspeckit</span><span class="o">.</span><span class="n">Spectrum</span><span class="p">(</span><span class="s1">&#39;iraf_spectrum.ms.fits&#39;</span><span class="p">)</span>
<span class="n">sp</span><span class="o">.</span><span class="n">plotter</span><span class="p">()</span>
</pre></div>
</div>
</section>
<section id="fitting-line-profiles">
<h2>Fitting Line Profiles<a class="headerlink" href="#fitting-line-profiles" title="Permalink to this heading">¶</a></h2>
<div class="admonition note">
<p class="admonition-title">Note</p>
<p>See <a class="reference internal" href="interactive.html"><span class="doc">A guide to interactive fitting</span></a> for a comprehensive graphical demonstration of these instructions.</p>
</div>
<p>In IRAF, a line profile is fitted using <em>k</em> to start the fitter, then <em>k</em>, <em>l</em>, or <em>v</em>
to perform the fit.</p>
<p>In PySpecKit, the continuum (<em>baseline</em>) and line profile are determined separately.</p>
<p>Instead of using a key twice to specify the continuum level, a continuum must
be fitted from the data.  This is done by pressing <em>b</em> to turn on the baseline fitter.
Click or press <em>1</em> to select baseline regions - they will be highlighted in green.
Press <em>3</em> to fit the baseline and display it as an orange line.</p>
<p>In PySpecKit, the interactive fitter is started by pressing <em>f</em> in the plot
window.  After pressing <em>f</em>, instructions will be provided in the terminal
window telling you which line profiles are implemented.  Select one of these,
or use a gaussian by default.</p>
<p>Select the line fitting region by pressing <em>1</em> on either side of the line.
Select the peak and full-width-half-maximum of the line by pressing <em>2</em> at each
of these locations in turn.  You may repeat this cycle indefinitely to fit
multiple profiles (comparable to IRAF*s <em>deblend</em> capability).  Then, press <em>3</em>
to perform the fit.</p>
<p>The fitted parameters can be accessed (as variables, or printed) through the
<code class="xref py py-obj docutils literal notranslate"><span class="pre">Spectrum.specfit.parinfo</span></code> parameter.  They will also be displayed in the plot
legend.</p>
</section>
</section>
<section class="edit-section">
<p class="edit-on-github-para"><a class="reference external" href="http://github.com/pyspeckit/pyspeckit/tree/master/docs/guide_iraf.rst" title="Push the Edit button on the next page"><span class="edit-on-github">[edit this page on github]</span></a></p>
<p class="edit-on-bitbucket-para"><a class="reference external" href="http://bitbucket.org/pyspeckit/pyspeckit/src/tip/doc/guide_iraf.rst" title="Push the Edit button on the next page"><span class="edit-on-bitbucket">[edit this page on bitbucket]</span></a></p>
</section>


            <div class="clearer"></div>
          </div>
        </div>
      </div>
        </div>
        <div class="sidebar">
          <h3>Table Of Contents</h3>
          <ul class="current">
<li class="toctree-l1"><a class="reference internal" href="index.html">Index</a></li>
<li class="toctree-l1"><a class="reference internal" href="install.html">Installation and Requirements</a></li>
<li class="toctree-l1"><a class="reference internal" href="models.html">Models &amp; Fitting</a></li>
<li class="toctree-l1"><a class="reference internal" href="plotting.html">Basic Plotting Guide</a></li>
<li class="toctree-l1"><a class="reference internal" href="features.html">Class Features</a></li>
<li class="toctree-l1"><a class="reference internal" href="readers.html">File Readers</a></li>
<li class="toctree-l1"><a class="reference internal" href="wrappers.html">High-Level Wrappers</a></li>
<li class="toctree-l1 current"><a class="reference internal" href="examples.html">Examples</a><ul class="current">
<li class="toctree-l2"><a class="reference internal" href="example_fromscratch.html">Build a spectrum from scratch (and fit a gaussian)</a></li>
<li class="toctree-l2"><a class="reference internal" href="example_continuum_fromscratch.html">Build a spectrum from scratch (and fit the continuum)</a></li>
<li class="toctree-l2"><a class="reference internal" href="example_minimal_cube.html">Minimal gaussian cube fitting</a></li>
<li class="toctree-l2"><a class="reference internal" href="example_cube_fromscratch.html">Build a cube from scratch and fit many gaussians</a></li>
<li class="toctree-l2"><a class="reference internal" href="example_h2co.html">Radio H2CO</a></li>
<li class="toctree-l2"><a class="reference internal" href="example_h2co_mm.html">Radio H2CO mm lines</a></li>
<li class="toctree-l2"><a class="reference internal" href="example_nh3.html">Radio NH3: shows hyperfine fitting with fixed amplitude, width</a></li>
<li class="toctree-l2"><a class="reference internal" href="example_nh3_cube.html">Radio NH3 cube</a></li>
<li class="toctree-l2"><a class="reference internal" href="nh3_tau.html">Radio NH3 - fit N hyperfines with no temperature</a></li>
<li class="toctree-l2"><a class="reference internal" href="example_n2hp.html">Radio N2H+</a></li>
<li class="toctree-l2"><a class="reference internal" href="example_n2hp_cube.html">Radio N2H+ cube</a></li>
<li class="toctree-l2"><a class="reference internal" href="example_o-nh2d.html">Radio ortho-NH2D</a></li>
<li class="toctree-l2"><a class="reference internal" href="example_p-nh2d.html">Radio para-NH2D</a></li>
<li class="toctree-l2"><a class="reference internal" href="example_hcn.html">Radio HCN: shows hyperfine with varying amplitude &amp; width</a></li>
<li class="toctree-l2"><a class="reference internal" href="example_hcop.html">Radio HCO+</a></li>
<li class="toctree-l2"><a class="reference internal" href="example_LTE.html">Radio LTE Line Forest model</a></li>
<li class="toctree-l2"><a class="reference internal" href="example_NIR_cube.html">Near IR cube</a></li>
<li class="toctree-l2"><a class="reference internal" href="example_sdss.html">Optical SDSS</a></li>
<li class="toctree-l2"><a class="reference internal" href="example_vega_echelle.html">Optical Vega Echelle</a></li>
<li class="toctree-l2"><a class="reference internal" href="interactive.html">Optical INTERACTIVE</a></li>
<li class="toctree-l2"><a class="reference internal" href="example_sn.html">Optical Supernova (?) H-alpha multi-component voigt</a></li>
<li class="toctree-l2"><a class="reference internal" href="example_MUSE.html">Optical MUSE Spectral Cube</a></li>
<li class="toctree-l2"><a class="reference internal" href="example_template.html">Template Fitting</a></li>
<li class="toctree-l2"><a class="reference internal" href="example_pymc.html">Monte Carlo</a></li>
<li class="toctree-l2"><a class="reference internal" href="example_pymc_ammonia.html">Monte Carlo for NH3</a></li>
<li class="toctree-l2"><a class="reference internal" href="guide_class.html">CLASS Guide</a></li>
<li class="toctree-l2 current"><a class="current reference internal" href="#">IRAF Guide</a><ul>
<li class="toctree-l3"><a class="reference internal" href="#loading-a-spectrum">Loading a Spectrum</a></li>
<li class="toctree-l3"><a class="reference internal" href="#fitting-line-profiles">Fitting Line Profiles</a></li>
</ul>
</li>
<li class="toctree-l2"><a class="reference internal" href="projects.html">Student Projects</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="interactive.html">Interactive Use</a></li>
<li class="toctree-l1"><a class="reference internal" href="faq.html">FAQ</a></li>
</ul>

          <h3 style="margin-top: 1.5em;">Search</h3>
          <form class="search" action="search.html" method="get">
            <input type="text" name="q" />
            <input type="submit" value="Go" />
            <input type="hidden" name="check_keywords" value="yes" />
            <input type="hidden" name="area" value="default" />
          </form>
          <p class="searchtip" style="font-size: 90%">
            Enter search terms or a module, class or function name.
          </p>
        </div>
        <div class="clearer"></div>
      </div>
    </div>

    <div class="footer-wrapper">
      <div class="footer">
        <div class="left">
          <a href="guide_class.html" title="Guide for GILDAS-CLASS users"
             >previous</a> |
          <a href="projects.html" title="PySpecKit Projects"
             >next</a> |
          <a href="py-modindex.html" title="Python Module Index"
             >modules</a> |
          <a href="genindex.html" title="General Index"
             >index</a>
            <br/>
            <a href="_sources/guide_iraf.rst.txt"
               rel="nofollow">Show Source</a>
        </div>

        <div class="right">
          
    <div class="footer" role="contentinfo">
        &#169; Copyright 2023, Adam Ginsburg.
      Last updated on Apr 02, 2023.
      Created using <a href="https://www.sphinx-doc.org/">Sphinx</a> 6.1.3.
    </div>
        </div>
        <div class="clearer"></div>
      </div>
    </div>

  </body>
</html>